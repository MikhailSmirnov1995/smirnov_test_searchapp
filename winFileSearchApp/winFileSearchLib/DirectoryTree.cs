﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security;

namespace winFileSearchLib
{
    /*
     * Дерево каталогов и файлы: Обход рекурсивный и поиск по шаблону
     */
    public class DirectoryTree
    {
        String baseDirName;
        String fileNameTemplate;

        public DirectoryTree(String baseDirName, String fileNameTemplate)
        {
            this.baseDirName = baseDirName;
            this.fileNameTemplate = fileNameTemplate;
        }


        /// <summary>
        /// Вызов при нахождении файла, который соответствует шаблону fileNameTemplate
        /// </summary>
        /// <param name="file"> FileInfo </param>
        public Action<FileInfo> OnFoundFile;
        /// <summary>
        /// При возникновении исключений вызов SecurityException и DirectoryNotFoundException
        /// </summary>
        public Action<Exception> OnException;

        /// <summary>
        /// Запустк обхода дерева каталогов
        /// </summary>
        public void run()
        {
            DirectoryInfo baseDir = new DirectoryInfo(baseDirName);
            run(baseDir);
        }

        private void run(DirectoryInfo baseDir)
        {
            try
            {
                foreach (var file in baseDir.EnumerateFiles(fileNameTemplate))
                {
                    if (OnFoundFile != null)
                        OnFoundFile(file);
                }
                foreach (var dir in baseDir.EnumerateDirectories())
                {
                    run(dir);
                }
            }
            catch (SecurityException e)
            {
                if (OnException != null)
                    OnException(e);
            }
            catch (DirectoryNotFoundException e)
            {
                if (OnException != null)
                    OnException(e);
            }
            catch (System.UnauthorizedAccessException e)
            {
                if (OnException != null)
                    OnException(e);
            }
        }
    }
}
